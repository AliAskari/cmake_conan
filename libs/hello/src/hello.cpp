#include <iostream>
#include "hello.h"

void say_hello(const std::string& name)
{
    std::cout << "Hello " << name << std::endl;
}
